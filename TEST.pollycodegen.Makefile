##===- TEST.nightly.Makefile ------------------------------*- Makefile -*--===##
#
# This test is used in conjunction with the llvm/utils/NightlyTest* stuff to
# generate information about program status for the nightly report.
#
##===----------------------------------------------------------------------===##

CURDIR  := $(shell cd .; pwd)
PROGDIR := $(PROJ_SRC_ROOT)
RELDIR  := $(subst $(PROGDIR),,$(CURDIR))
POLLY := $(LLVM_OBJ_ROOT)/tools/polly/$(CONFIGURATION)/lib/LLVMPolly.so

OPTBETAOPTIONS := -load $(POLLY) -mem2reg -loopsimplify -indvars -polly-prepare -basicaa  -scev-aa -polly-cloog -polly-codegen

REPORTS_TO_GEN := nat
REPORT_DEPENDENCIES :=
REPORTS_TO_GEN +=  llc compile
REPORT_DEPENDENCIES += $(LLC) $(LOPT) $(POLLY)
REPORTS_TO_GEN += opt-beta compile
REPORT_DEPENDENCIES += $(LLC) $(LOPT)
REPORTS_SUFFIX := $(addsuffix .report.txt, $(REPORTS_TO_GEN))

# Compilation tests
$(PROGRAMS_TO_TEST:%=Output/%.nightly.compile.report.txt): \
Output/%.nightly.compile.report.txt: Output/%.llvm.bc
	@echo > $@
	@-if test -f Output/$*.linked.bc.info; then \
	  echo "TEST-PASS: compile $(RELDIR)/$*" >> $@;\
	  printf "TEST-RESULT-compile: " >> $@;\
	  grep "Total Execution Time" Output/$*.linked.bc.info | tail -n 1 >> $@;\
	  echo >> $@;\
	  printf "TEST-RESULT-compile: " >> $@;\
	  wc -c $< >> $@;\
	  echo >> $@;\
	else \
	  echo "TEST-FAIL: compile $(RELDIR)/$*" >> $@;\
	fi

# NAT tests
$(PROGRAMS_TO_TEST:%=Output/%.nightly.nat.report.txt): \
Output/%.nightly.nat.report.txt: Output/%.out-nat
	@echo > $@
	@printf "TEST-RESULT-nat-time: " >> $@
	-grep "^program" Output/$*.out-nat.time >> $@

# OPT experimental tests
$(PROGRAMS_TO_TEST:%=Output/%.nightly.opt-beta.report.txt): \
Output/%.nightly.opt-beta.report.txt: Output/%.exe-opt-beta
	@echo > $@
	@-if test -f Output/$*.exe-opt-beta; then \
	  head -n 100 Output/$*.exe-opt-beta >> $@; \
	  echo "TEST-PASS: opt-beta $(RELDIR)/$*" >> $@;\
	  printf "TEST-RESULT-opt-beta: " >> $@;\
	  grep "Total Execution Time" Output/$*.opt-beta.s.info | tail -n 1 >> $@;\
	  printf "TEST-RESULT-opt-beta-time: " >> $@;\
	  grep "^program" Output/$*.out-opt-beta.time >> $@;\
	  echo >> $@;\
	else  \
	  echo "TEST-FAIL: opt-beta $(RELDIR)/$*" >> $@;\
	fi

# LLC tests
$(PROGRAMS_TO_TEST:%=Output/%.nightly.llc.report.txt): \
Output/%.nightly.llc.report.txt: Output/%.exe-llc
	@echo > $@
	@-if test -f Output/$*.exe-llc; then \
	  head -n 100 Output/$*.exe-llc >> $@; \
	  echo "TEST-PASS: llc $(RELDIR)/$*" >> $@;\
	  printf "TEST-RESULT-llc: " >> $@;\
	  grep "Total Execution Time" Output/$*.llc.s.info | tail -n 1 >> $@;\
	  printf "TEST-RESULT-llc-time: " >> $@;\
	  grep "^program" Output/$*.out-llc.time >> $@;\
	  echo >> $@;\
	else  \
	  echo "TEST-FAIL: llc $(RELDIR)/$*" >> $@;\
	fi

# Overall tests: just run subordinate tests
$(PROGRAMS_TO_TEST:%=Output/%.$(TEST).report.txt): \
Output/%.$(TEST).report.txt: $(addprefix Output/%.nightly., $(REPORTS_SUFFIX))
	$(VERB) $(RM) -f $@
	@echo "---------------------------------------------------------------" >> $@
	@echo ">>> ========= '$(RELDIR)/$*' Program" >> $@
	@echo "---------------------------------------------------------------" >> $@
	-cat $(addprefix Output/$*.nightly., $(REPORTS_SUFFIX)) >> $@



$(PROGRAMS_TO_TEST:%=test.$(TEST).%): \
test.$(TEST).%: Output/%.$(TEST).report.txt
	@-cat $<
