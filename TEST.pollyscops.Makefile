##===- TEST.pollyscops.Makefile ------------------------------*- Makefile -*-===##
#
# This recursively traverses the programs, and runs the -polly-scop-info pass
# on each *.llvm.bc bytecode so that it is possible to tell us if polly-scop-info 
# pass the testsuite and do some statistics on extracted SCoPs and regions that not
# a legal part of SCoP 
# 
# Usage: 
#     make TEST=pollyscops quiet (do not output anything)
#     make TEST=pollyscops (detailed list with time passes, etc.)
#     make TEST=pollyscops report
#     make TEST=pollyscops report.html
#
##===----------------------------------------------------------------------===##

CURDIR  := $(shell cd .; pwd)
PROGDIR := $(PROJ_SRC_ROOT)
RELDIR  := $(subst $(PROGDIR),,$(CURDIR))
POLLY := $(LLVM_OBJ_ROOT)/tools/polly/$(CONFIGURATION)/lib/LLVMPolly.so
SCOP_OPTS = -load $(POLLY)  -mem2reg -loopsimplify -indvars -polly-prepare -basicaa  -scev-aa -polly-analyze-ir -disable-output

$(PROGRAMS_TO_TEST:%=test.$(TEST).%): \
test.$(TEST).%: Output/%.$(TEST).report.txt
	@cat $<

$(PROGRAMS_TO_TEST:%=Output/%.$(TEST).report.txt):  \
Output/%.$(TEST).report.txt: Output/%.llvm.bc $(LOPT) \
	$(PROJ_SRC_ROOT)/TEST.pollyscops.Makefile 
	$(VERB) $(RM) -f $@
	@echo "---------------------------------------------------------------" >> $@
	@echo ">>> ========= '$(RELDIR)/$*' Program" >> $@
	@echo "---------------------------------------------------------------" >> $@
	@-$(LOPT) $(SCOP_OPTS) -stats $< 2>>$@ 
quiet:
	@-$(LOPT) $(SCOP_OPTS) $< 2>>$@ 

.PHONY: quiet
REPORT_DEPENDENCIES := $(LOPT) $(POLLY)
